<?php
/**
 * The base template for displaying 404 pages (not found).
 *
 * @package Irozo
 */
?>
<?php get_header( irozo_template_base() ); ?>

	<?php irozo_site_breadcrumbs(); ?>

	<div <?php irozo_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php irozo_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include irozo_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php. ?>

		</div><!-- .row -->

	</div><!-- .site-content_wrap -->

<?php get_footer( irozo_template_base() ); ?>
