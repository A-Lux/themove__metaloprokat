<?php
/**
 * The template for displaying the style-3 footer layout.
 *
 * @package Irozo
 */
?>

<div <?php irozo_footer_container_class(); ?>>
	<div class="site-info container-wide">
		<div class="site-info-wrap">
			<div class="site-info-block"><?php
				irozo_footer_logo();
				irozo_footer_copyright();
			?></div>
			<?php irozo_footer_menu(); ?>
			<div class="site-info-block"><?php
				irozo_contact_block( 'footer' );
				irozo_social_list( 'footer' );
			?></div>
		</div>
	</div><!-- .site-info -->
</div><!-- .container -->
