<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package Irozo
 */
?>

<div <?php irozo_footer_container_class(); ?>>
	<div class="site-info container"><?php
		irozo_footer_logo();
		irozo_footer_menu();
		irozo_contact_block( 'footer' );
		irozo_social_list( 'footer' );
		irozo_footer_copyright();
	?></div><!-- .site-info -->
</div><!-- .container -->
