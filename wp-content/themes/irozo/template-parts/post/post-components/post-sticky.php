<?php
/**
 * Template part for displaying sticky.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Irozo
 */

$utility          = irozo_utility()->utility;
$sticky           = irozo_sticky_label( false );

echo $sticky;
