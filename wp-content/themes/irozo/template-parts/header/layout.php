<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Irozo
 */

$header_contact_block_visibility = get_theme_mod( 'header_contact_block_visibility', irozo_theme()->customizer->get_default( 'header_contact_block_visibility' ) );
$header_btn_visibility           = get_theme_mod( 'header_btn_visibility', irozo_theme()->customizer->get_default( 'header_btn_visibility' ) );
$search_visible                  = get_theme_mod( 'header_search', irozo_theme()->customizer->get_default( 'header_search' ) );
$header_woo_elements             = get_theme_mod( 'header_woo_elements', irozo_theme()->customizer->get_default( 'header_woo_elements' ) );
?>
<div class="header-container_wrap container">

	<?php if ( $header_contact_block_visibility || $header_btn_visibility ) : ?>
		<div class="header-row__flex header-components__contact-button header-components__grid-elements"><?php
			irozo_contact_block( 'header' );
			irozo_header_btn();
		?></div>
	<?php endif; ?>

	<div class="header-container__flex-wrap">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php irozo_header_logo() ?>
				<?php irozo_site_description(); ?>
			</div>

			<div class="header-nav-wrapper">
				<?php irozo_main_menu(); ?>

				<?php if ( $search_visible || $header_woo_elements ) : ?>
					<div class="header-components header-components__search-cart"><?php
						irozo_header_search_toggle();
						irozo_header_woo_cart();
					?></div>
				<?php endif; ?>

			</div>
		</div>

		<?php irozo_header_search( '<div class="header-search">%s<span class="search-form__close"></span></div>' ); ?>
	</div>
</div>
