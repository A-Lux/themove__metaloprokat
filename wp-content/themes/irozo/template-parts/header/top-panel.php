<?php
/**
 * Template part for top panel in header.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Irozo
 */

// Don't show top panel if all elements are disabled.
if ( ! irozo_is_top_panel_visible() ) {
	return;
}
?>

<div <?php echo irozo_get_html_attr_class( array( 'top-panel' ), 'top_panel_bg' ); ?>>
	<div class="container">
		<div class="top-panel__container">
			<?php irozo_top_message( '<div class="top-panel__message">%s</div>' ); ?>
			<?php irozo_contact_block( 'header_top_panel' ); ?>

			<div class="top-panel__wrap-items">
				<div class="top-panel__menus">
					<?php irozo_top_menu(); ?>
					<?php irozo_login_link(); ?>
					<?php irozo_header_woo_currency_switcher(); ?>
					<?php irozo_social_list( 'header' ); ?>
				</div>
			</div>
		</div>
	</div>
</div><!-- .top-panel -->
