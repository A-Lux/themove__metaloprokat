<?php
/**
 * Template part for style-3 header layout.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Irozo
 */

$vertical_menu_slide             = ( ! is_rtl() ) ? 'right' : 'left';
$header_contact_block_visibility = get_theme_mod( 'header_contact_block_visibility', irozo_theme()->customizer->get_default( 'header_contact_block_visibility' ) );
$header_btn_visibility           = get_theme_mod( 'header_btn_visibility', irozo_theme()->customizer->get_default( 'header_btn_visibility' ) );
$search_visible                  = get_theme_mod( 'header_search', irozo_theme()->customizer->get_default( 'header_search' ) );
$header_woo_elements             = get_theme_mod( 'header_woo_elements', irozo_theme()->customizer->get_default( 'header_woo_elements' ) );

?>
<div class="header-container_wrap container">
	<?php irozo_vertical_main_menu( $vertical_menu_slide ); ?>

	<?php if ( $header_contact_block_visibility || $header_btn_visibility ) : ?>
		<div class="header-row__flex header-components__contact-button header-components__grid-elements"><?php
			irozo_contact_block( 'header' );
			irozo_header_btn();
		?></div>
	<?php endif; ?>

	<div class="header-container__flex-wrap">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php irozo_header_logo() ?>
				<?php irozo_site_description(); ?>
			</div>

			<div class="header-components header-components__search-cart"><?php
				irozo_header_search_toggle();
				irozo_header_woo_cart();
				irozo_vertical_menu_toggle( 'main-menu' );
			?></div>
		</div>

		<?php irozo_header_search( '<div class="header-search">%s<span class="search-form__close"></span></div>' ); ?>
	</div>
</div>
