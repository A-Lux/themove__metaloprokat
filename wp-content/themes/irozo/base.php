<?php
/**
 * The base template.
 *
 * @package Irozo
 */
?>
<?php get_header( irozo_template_base() ); ?>

	<?php irozo_site_breadcrumbs(); ?>

	<?php do_action( 'irozo_render_widget_area', 'full-width-header-area' ); ?>

	<div <?php irozo_content_wrap_class(); ?>>

		<?php do_action( 'irozo_render_widget_area', 'before-content-area' ); ?>

		<div class="row">

			<div id="primary" <?php irozo_primary_content_class(); ?>>

				<?php do_action( 'irozo_render_widget_area', 'before-loop-area' ); ?>

				<main id="main" class="site-main" role="main">

					<?php include irozo_template_path(); ?>

				</main><!-- #main -->

				<?php do_action( 'irozo_render_widget_area', 'after-loop-area' ); ?>

			</div><!-- #primary -->

			<?php irozo_get_sidebar(); ?>

		</div><!-- .row -->

		<?php do_action( 'irozo_render_widget_area', 'after-content-area' ); ?>

	</div><!-- .site-content_wrap -->

	<?php do_action( 'irozo_render_widget_area', 'after-content-full-width-area' ); ?>

<?php get_footer( irozo_template_base() ); ?>
