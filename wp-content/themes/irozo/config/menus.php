<?php
/**
 * Menus configuration.
 *
 * @package Irozo
 */

add_action( 'after_setup_theme', 'irozo_register_menus', 5 );
/**
 * Register menus.
 */
function irozo_register_menus() {

	register_nav_menus( array(
		'top'          => esc_html__( 'Top', 'irozo' ),
		'main'         => esc_html__( 'Main', 'irozo' ),
		'main_landing' => esc_html__( 'Landing Main', 'irozo' ),
		'footer'       => esc_html__( 'Footer', 'irozo' ),
		'social'       => esc_html__( 'Social', 'irozo' ),
	) );
}
