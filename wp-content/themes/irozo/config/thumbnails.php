<?php
/**
 * Thumbnails configuration.
 *
 * @package Irozo
 */

add_action( 'after_setup_theme', 'irozo_register_image_sizes', 5 );
/**
 * Register image sizes.
 */
function irozo_register_image_sizes() {
	set_post_thumbnail_size( 360, 203, true );

	// Registers a new image sizes.
	add_image_size( 'irozo-thumb-s', 150, 150, true );
	add_image_size( 'irozo-thumb-m', 460, 460, true );
	add_image_size( 'irozo-thumb-l', 660, 371, true );
	add_image_size( 'irozo-thumb-l-2', 766, 203, true );
	add_image_size( 'irozo-thumb-xl', 1160, 508, true );

	add_image_size( 'irozo-thumb-masonry', 560, 9999 );

	add_image_size( 'irozo-slider-thumb', 150, 86, true );

	add_image_size( 'irozo-woo-cart-product-thumb', 104, 104, true );
	add_image_size( 'irozo-thumb-listing-line-product', 260, 260, true );

	add_image_size( 'irozo-thumb-260-147', 260, 147, true );
	add_image_size( 'irozo-thumb-480-271', 480, 271, true );
	add_image_size( 'irozo-thumb-560-315', 560, 315, true );
	add_image_size( 'irozo-thumb-760-571', 760, 571, true );
}
