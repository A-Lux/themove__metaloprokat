<?php
/**
 * TM-Wizard configuration.
 *
 * @var array
 *
 * @package Irozo
 */

$plugins = array(
	'cherry-data-importer' => array(
		'name'   => esc_html__( 'Cherry Data Importer', 'irozo' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/CherryFramework/cherry-data-importer/archive/master.zip',
		'access' => 'skins',
	),
	'cherry-projects' => array(
		'name'   => esc_html__( 'Cherry Projects', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-popups' => array(
		'name'   => esc_html__( 'Cherry PopUps', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-team-members' => array(
		'name'   => esc_html__( 'Cherry Team Members', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-testi' => array(
		'name'   => esc_html__( 'Cherry Testimonials', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-search' => array(
		'name'   => esc_html__( 'Cherry Search', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-services-list' => array(
		'name'   => esc_html__( 'Cherry Services List', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-sidebars' => array(
		'name'   => esc_html__( 'Cherry Sidebars', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-socialize' => array(
		'name'   => esc_html__( 'Cherry Socialize', 'irozo' ),
		'access' => 'skins',
	),
	'cherry-trending-posts' => array(
		'name'   => esc_html__( 'Cherry Trending Posts', 'irozo' ),
		'access' => 'skins',
	),
	'booked' => array(
		'name'   => esc_html__( 'Booked Appointments', 'irozo' ),
		'source' => 'local',
		'path'   => IROZO_THEME_DIR . '/assets/includes/plugins/booked.zip',
		'access' => 'skins',
	),
	'elementor' => array(
		'name'   => esc_html__( 'Elementor Page Builder', 'irozo' ),
		'access' => 'skins',
	),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements addon For Elementor', 'irozo' ),
		'source' => 'local',
		'path'   => IROZO_THEME_DIR . '/assets/includes/plugins/jet-elements.zip',
		'access' => 'skins',
	),
		'tm-mega-menu' => array(
		'name'   => esc_html__( 'TM Mega Menu', 'irozo' ),
		'source' => 'remote',
		'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/tm-mega-menu.zip',
		'access' => 'skins',
	),
	'tm-photo-gallery' => array(
		'name'   => esc_html__( 'TM Photo Gallery', 'irozo' ),
		'access' => 'skins',
	),
	'tm-timeline' => array(
		'name'   => esc_html__( 'TM Timeline', 'irozo' ),
		'access' => 'skins',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'irozo' ),
		'access' => 'skins',
	),
	'simple-file-downloader' => array(
		'name'   => esc_html__( 'Simple File Downloader', 'irozo' ),
		'access' => 'skins',
	),
	'shortcode-widget' => array(
		'name'   => esc_html__( 'Shortcode Widget', 'irozo' ),
		'access' => 'skins',
	),
	'woocommerce' => array(
		'name'   => esc_html__( 'Woocommerce', 'irozo' ),
		'access' => 'skins',
	),
	'tm-woocommerce-ajax-filters' => array(
		'name'   => esc_html__( 'TM Woocommerce Ajax Filters', 'irozo' ),
		'source' => 'local',
		'path'   => IROZO_THEME_DIR . '/assets/includes/plugins/tm-woocommerce-ajax-filters.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-compare-wishlist' => array(
		'name'   => esc_html__( 'TM Woocommerce Compare Wishlist', 'irozo' ),
		'access' => 'skins',
	),
	'tm-woocommerce-package' => array(
		'name'   => esc_html__( 'TM Woocommerce Package', 'irozo' ),
		'access' => 'skins',
	),
	'tm-woocommerce-quick-view' => array(
		'name'   => esc_html__( 'TM WooCommerce Quick View', 'irozo' ),
		'source' => 'local',
		'path'   => IROZO_THEME_DIR . '/assets/includes/plugins/tm-woocommerce-quick-view.zip',
		'access' => 'skins',
	),
	'woocommerce-social-media-share-buttons' => array(
		'name'   => esc_html__( 'Woocommerce Social Media Share Buttons', 'irozo' ),
		'access' => 'skins',
	),
	'smart-slider-3' => array(
		'name'   => esc_html__( 'Smart Slider 3', 'irozo' ),
		'access' => 'skins',
	),
	'wordpress-social-login' => array(
		'name'   => esc_html__( 'WordPress Social Login', 'irozo' ),
		'access' => 'skins',
	),	 
);

/**
 * Skins configuration.
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'cherry-data-importer',
		'elementor',
		'jet-elements',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'booked',
				'cherry-projects',
				'cherry-popups',
				'cherry-search',
				'cherry-services-list',
				'cherry-sidebars',
				'cherry-socialize',
				'cherry-team-members',
				'cherry-testi',
				'cherry-trending-posts',
				'tm-mega-menu',
				'tm-photo-gallery',
				'tm-timeline',
				'contact-form-7',
				'simple-file-downloader',
				'smart-slider-3',
				'shortcode-widget',
				'woocommerce',
				'tm-woocommerce-ajax-filters',
				'tm-woocommerce-compare-wishlist',
				'tm-woocommerce-package',
				'woocommerce-social-media-share-buttons',
				'tm-woocommerce-quick-view',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'https://ld-wp73.template-help.com/wordpress/prod_11368/v1',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default-thumb.png',
			'name'  => esc_html__( 'Irozo', 'irozo' ),
		),
	),
);

$texts = array(
	'theme-name' => esc_html__( 'Irozo', 'irozo' ),
);
