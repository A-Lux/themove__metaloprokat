<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'metaloprokat' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8|unwHL9I-L(x9#8}#=Y4L9.Jg0<kw[8FeoLRA)E,2?S{%l/h`l+o}Gon5EsV*rm' );
define( 'SECURE_AUTH_KEY',  'DB1EP({}eL4Cl!wX QEbGILvwzd0aG;u[DP3,/7<xrN^k9vLrvQ(s.RCUBfPRsbC' );
define( 'LOGGED_IN_KEY',    '&ylR kdhw$d*_!zp2>-92rPhEs?_3K3=0(Y/MNrO=5oRbFiqwY)(Y>?3x<iP6%Nk' );
define( 'NONCE_KEY',        'K$e+&[2M5O8!J_E}$k,#ME;R X{U7>cn>s97U0Z gk:pd<.-#t{xF[blytb9kmK$' );
define( 'AUTH_SALT',        'UG`w[4P_v?ds4$.[jTEI@h0@d^_e<+).#>$7lT;/YYI#H137M|+aq})BJ(Of4&pl' );
define( 'SECURE_AUTH_SALT', '{C&$AxUa+M)+}3O[QYplOOR)xXT@0=xsMLUgYA:s?8uX,CY($>1>E:|P@>9[1Ojj' );
define( 'LOGGED_IN_SALT',   'T>z(x(>,#nE(}cG(aj|(TglEFxT#LGRmC;TBz!u=.cU50iQ@+*5+<X_.Q?#hb |p' );
define( 'NONCE_SALT',       'l~F5&>&3:{8y(b%HP.QD[S}PU.xJW93|(T<6gF4>P08V=~]pC:G^c3Z6:|976~]{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
